﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
	public partial class FrostGUI
	{
		private void ProcessStart(string arguments)
		{
			string filename = Path.GetFileName((string)FileList.Items[0]);
			CurrentRender.Text = "Rendering " + filename;

			ProcessStartInfo si = new ProcessStartInfo();
			{
				si.Arguments = arguments;

				si.FileName = "ffmpeg.exe";
				si.UseShellExecute = false;
				si.RedirectStandardOutput = true;
				si.RedirectStandardInput = true;
				si.RedirectStandardError = true;
				si.CreateNoWindow = true;
				si.WindowStyle = ProcessWindowStyle.Hidden;
			}

			_cmd = new Process();
			_cmd.StartInfo = si;

			if (_cmd.Start())
			{
				_cmd.EnableRaisingEvents = true;
				_cmd.OutputDataReceived += new DataReceivedEventHandler(_cmd_OutputDataReceived);
				_cmd.ErrorDataReceived += new DataReceivedEventHandler(_cmd_ErrorDataReceived);
				_cmd.Exited += new EventHandler(_cmd_Exited);

				_cmd.BeginOutputReadLine();
				_cmd.BeginErrorReadLine();
			}
			else
			{
				_cmd = null;
			}
		}
	}
}