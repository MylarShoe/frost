﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
	public partial class FrostGUI
	{
		Process _cmd;
		int H265_Pass_Value = -1;

		private void StartButton_Click(object sender, EventArgs e)
		{
			if (!File.Exists("FFmpeg.exe"))
			{
				MessageBox.Show("FFmpeg not found. Cannot complete\nrender until installed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}

			if (StartButton.Text == "Stop")
			{
				_cmd.Kill();
				RenderEnd();
				CommandWindow.Items.Clear();
				CommandWindow.Items.Add("Render Stopped");
				return;
			}
			else
			{
				if (FileList.Items.Count == 0)
				{
					MessageBox.Show("No file in render queue", "Error");
					return;
				}
				else if (DestinationList.Items.Count == 0)
				{
					MessageBox.Show("No output destination selected", "Error");
					return;
				}
				else
				{
					RenderStart();
				}
			}

			string filename = Path.GetFileName((string)FileList.Items[0]);
			string ext = Path.GetExtension((string)FileList.Items[0]);
			string filenameMKV = filename.Remove(filename.Length - 4, 4) + ".mkv";

			//ProRes Start
			if (tabControl1.SelectedTab == ProRes)
			{
				if (FileExists(filename, (string)DestinationList.Items[0], ".mov"))
				{
					MessageBox.Show("File already exists. Please change destination", "Error");
					RenderEnd();
					return;
				}

				ProcessStart(SoloPrepProRes());
			}
			//H.265 Start
			else if (tabControl1.SelectedTab == H265)
			{
				if (ext == ".mp4" || ext == ".MP4") { }
				else
				{
					MessageBox.Show("Incompatiable File Format\nOnly .mp4 files accepted", "Error");
					RenderEnd();
					return;
				}
				if (FileExists(filename, (string)DestinationList.Items[0], ".mp4") && MP4_Button.Checked == true)
				{
					MessageBox.Show("File already exists. Please change destination", "Error");
					RenderEnd();
					return;
				}
				else if (FileExists(filenameMKV, (string)DestinationList.Items[0], ".mkv") && MKV_Button.Checked == true)
				{
					MessageBox.Show("File already exists. Please change destination", "Error");
					RenderEnd();
					return;
				}

				H265_Pass_Value = 1;

				ProcessStart(SoloPrepH265());
			}
			else { MessageBox.Show("Internal Error", "Error"); }

			
		}
	}
}
