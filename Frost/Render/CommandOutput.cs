﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
	public partial class FrostGUI
	{
		delegate void SetTextCallback(string text);

		private void SetText(string text)
		{
			if (this.CommandWindow.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetText);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				this.CommandWindow.Items.Add(text + Environment.NewLine);
				this.CommandWindow.SelectedIndex = CommandWindow.Items.Count - 1;
				this.CommandWindow.SelectedIndex = -1;
			}
		}

		private void EndRender(string text)
		{
			if (this.CommandWindow.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(EndRender);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				this.RenderEnd();
			}
		}

		void _cmd_OutputDataReceived(object sender, DataReceivedEventArgs e)
		{
			UpdateConsole(e.Data);
		}

		void _cmd_ErrorDataReceived(object sender, DataReceivedEventArgs e)
		{
			UpdateConsole(e.Data, Brushes.Red);
		}

		void _cmd_Exited(object sender, EventArgs e)
		{
			_cmd.OutputDataReceived -= new DataReceivedEventHandler(_cmd_OutputDataReceived);
			_cmd.Exited -= new EventHandler(_cmd_Exited);

			if (this.StartButton.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(EndRender);
				this.Invoke(d, new object[] { "Start" });
			}
			else
			{
				this.RenderEnd();
			}

		}

		private void UpdateConsole(string text)
		{
			UpdateConsole(text, null);
		}
		private void UpdateConsole(string text, Brush color)
		{
			WriteLine(text, color);
		}

		private void WriteLine(string text, Brush color)
		{
			if (text != null)
			{
				SetText(text);
			}
		}
	}
}