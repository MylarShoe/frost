﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
	public partial class FrostGUI
	{
		private string SoloPrepProRes()
		{
			int _quality = PR_Quality.SelectedIndex;
			int _audio = PR_Audio.SelectedIndex;
			int _resolution = PR_Resolution.SelectedIndex;
			string audio;
			string resolution;
			string quality;

			#region Quality
			if (_quality == 0) {
				quality = "prores -profile 0";
			}
			else if (_quality == 1) {
				quality = "prores -profile 1";
			}
			else if (_quality == 2) {
				quality = "prores -profile 2";
			}
			else if (_quality == 3) {
				quality = "prores -profile 3";
			}
			else if (_quality == 4) {
				quality = "prores_ks -profile 4444 -pix_fmt yuva444p10le -alpha_bits 0";
			}
			else if (_quality == 5) {
				quality = "prores_ks -profile 4444 -pix_fmt yuva444p10le -alpha_bits 8";
			}
			else if (_quality == 6)
			{
				quality = "prores_ks -profile 4444 -pix_fmt yuva444p10le -alpha_bits 16";
			}
			else {
				quality = "prores -profile 2";
			}
			#endregion

			#region Audio
			if (_audio == 0) {
				audio = "-acodec copy";
			}
			else {
				audio = "-an";
			}
			#endregion

			#region Resolution
			if (_resolution == 0) {
				resolution = "";
			}
			else if (_resolution == 1) {
				resolution = "-s 1280x720";
			}
			else if (_resolution == 2) {
				resolution = "-s 1920x1080";
			}
			else if (_resolution == 3) {
				resolution = "-s 3840x2160";
			}
			else if (_resolution == 4) {
				resolution = "-s 4096x2160";
			}
			else {
				resolution = "";
			}
			#endregion

			string input = (string)FileList.Items[0];
			input = input.Replace("\\", "\\\\");
			string filename = Path.GetFileName((string)FileList.Items[0]);
			filename = filename.Remove(filename.Length - 4, 4);
			string output = (string)DestinationList.Items[0];
			output = output.Replace("\\", "\\\\");

			string arguments = "-i \"" + input + "\" -vcodec " + quality + " " + audio + " -dcodec copy " + resolution + " \"" + output + "\\" + filename + ".mov\"";

			return arguments;
		}
		private string SoloPrepH265()
		{
			int _quality = (int)H_Quality.Value;
			int _speed = H_Speed.SelectedIndex;
			int _audio = H_Audio.SelectedIndex;
			int _resolution = H_Resolution.SelectedIndex;
			string speed;
			string audio;
			string resolution;
			string ext;

			#region Audio
			if (_audio == 0) {
				audio = "-c:a copy";
			}
			else {
				audio = "-an";
			}
			#endregion

			#region SpeedVariable
			if (_speed == 0) {
				speed = "-preset:v ultrafast";
			}
			else if (_speed == 1) {
				speed = "-preset:v superfast";
			}
			else if (_speed == 2) {
				speed = "-preset:v veryfast";
			}
			else if (_speed == 3) {
				speed = "-preset:v faster";
			}
			else if (_speed == 4) {
				speed = "-preset:v fast";
			}
			else if (_speed == 5) {
				speed = "-preset:v medium";
			}
			else if (_speed == 6) {
				speed = "-preset:v slow";
			}
			else if (_speed == 7) {
				speed = "-preset:v slower";
			}
			else if (_speed == 8) {
				speed = "-preset:v veryslow";
			}
			else if (_speed == 9) {
				speed = "-preset:v placebo";
			}
			else {
				speed = "-preset:v medium";
			}
			#endregion

			#region Resolution
			if (_resolution == 0) {
				resolution = "";
			}
			else if (_resolution == 1) {
				resolution = "-s 1280x720";
			}
			else if (_resolution == 2) {
				resolution = "-s 1920x1080";
			}
			else if (_resolution == 3) {
				resolution = "-s 3840x2160";
			}
			else if (_resolution == 4) {
				resolution = "-s 4096x2160";
			}
			else {
				resolution = "";
			}
			#endregion

			#region Container
			if (MP4_Button.Checked == true) {
				ext = ".mp4";
			}
			else if (MKV_Button.Checked == true) {
				ext = ".mkv";
			}
			else {
				ext = ".mp4";
			}
			#endregion

			string input = (string)FileList.Items[0];
			input = input.Replace("\\", "\\\\");
			string filename = Path.GetFileName((string)FileList.Items[0]);
			filename = filename.Remove(filename.Length - 4, 4);
			string output = (string)DestinationList.Items[0];
			output = output.Replace("\\", "\\\\");

			string arguments = "-i \"" + input + "\" -c:v libx265 " + speed + " -x265-params crf=" + _quality + " " + audio + " " + resolution + " \"" + output + "\\" + filename + ext + "\"";

			return arguments;
		}
	}
}
