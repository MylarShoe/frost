﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Frost
{
	public partial class FrostGUI
	{
		private void SetDefaults()
		{
			PR_Quality.SelectedIndex = 2;
			PR_Audio.SelectedIndex = 0;
			PR_Resolution.SelectedIndex = 0;
			H_Quality.Text = "28";
			H_Speed.SelectedIndex = 5;
			H_Audio.SelectedIndex = 0;
			H_Resolution.SelectedIndex = 0;
			MP4_Button.Checked = true;
			CRF_Popup.SetToolTip(this.H_Quality_Text, "CRF is the Constant Rate Factor for H.265 video\nIt is a number between 0-51\nThe higher the number the lower quality\nThe default is 28");
		}

		private void RenderStart()
		{
			StartButton.Text = "Stop";
			CommandWindow.Items.Clear();
			tabControl1.Enabled = false;
			FileList.Enabled = false;
			DestinationList.Enabled = false;
			AddFiles.Enabled = false;
			RemoveFiles.Enabled = false;
			Destination_Select.Enabled = false;
			DestinationClear.Enabled = false;
			DestinationLock.Enabled = false;
		}

		private void RenderEnd()
		{
			if (tabControl1.SelectedTab == H265)
			{
				if (H265_Pass_Value == 1)
				{
					H265_Pass_Value = 2;
				}
				else if (H265_Pass_Value == 2)
				{
					H265_Pass_Value = -1;
				}
			}
			StartButton.Text = "Start";
			tabControl1.Enabled = true;
			FileList.Enabled = true;
			DestinationList.Enabled = true;
			AddFiles.Enabled = true;
			RemoveFiles.Enabled = true;
			Destination_Select.Enabled = true;
			DestinationClear.Enabled = true;
			DestinationLock.Enabled = true;
			FileList.Items.Clear();
			CurrentRender.Text = "Render Complete";
		}

		private int RenderChecker()
		{
			Process[] pname = Process.GetProcessesByName("ffmpeg");
			if (pname.Length == 0)
				return 0;
			else
				return 1;
		}

		private bool FileExists(string filename, string path, string ext)
		{
			filename = filename.Remove(filename.Length - 4, 4);
			filename = filename + ext;
			string filepath = path + "\\" + filename;
			if (File.Exists(filepath))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool ValidFormatExtension(string extension)
		{
			string[] supportedFormats = { ".mov", ".MOV", ".mp4", ".MP4", ".mxf", ".MXF", ".avi", ".AVI" };
			bool valid = false;

			for (int i=0; i<supportedFormats.Length; i++)
			{
				if (supportedFormats[i] == extension)
				{
					valid = true;
					break;
				}
			}
			return valid;
		}
	}
}
