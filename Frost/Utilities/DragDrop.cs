﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
	public partial class FrostGUI
	{
		private void FileList_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.All;
			else
				e.Effect = DragDropEffects.None;
		}

		private void FileList_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
			int i;
			for (i = 0; i < s.Length; i++)
			{
				if (Directory.Exists(s[i]))
				{
					MessageBox.Show("Cannot add folder. Only files are supported", "Error");
					continue;
				}
				if (ValidFormatExtension(Path.GetExtension(s[i])) == false)
				{
					MessageBox.Show("Unsupported File Type", "Import Error");
					continue;
				}
				else
				{
					if (FileList.Items.Contains(s[i]) == true)
					{
						MessageBox.Show("File is already in Queue", "Error");
						continue;
					}
					else
					{
						FileList.Items.Clear();
						FileList.Items.Add(s[i]);
					}
				}
			}
		}

		private void DestinationList_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.All;
			else
				e.Effect = DragDropEffects.None;
		}

		private void DestinationList_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
			int i;
			for (i = 0; i < s.Length; i++)
			{
				if (Directory.Exists(s[i]))
				{
					DestinationList.Items.Clear();
					DestinationList.Items.Add(s[i]);
				}
				else
				{
					if (DestinationList.Items.Contains(s[i]) == true)
					{
						MessageBox.Show("Only folders can be dropped here.", "Error");
						continue;
					}
					else
					{
						
					}
				}
			}
		}
	}
}
