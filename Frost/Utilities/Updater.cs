﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace Frost
{
	class Updater
	{
		public static void CheckUpdate()
		{
			string webData = GetFile();
			if (webData == "") { }
			else
			{
				Version version = new Version(Application.ProductVersion);
				string localVersion = version.ToString();

				char[] delimiters = { '\n' };
				string[] lines = webData.Split(delimiters);
				string webVersion = lines[0];
				string webLink = lines[1];

				if (webVersion == localVersion) { }
				else
				{
					if (MessageBox.Show("There is a new version. Would you like to update?", "Update Found", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						Process.Start(webLink);
					}
					else { }
				}
			}
		}

		private static string GetFile()
		{
			try
			{
				System.Net.WebClient wc = new System.Net.WebClient();
				string webData = wc.DownloadString("http://derekreinhardt.com/files/frost/currentversion.txt");
				return webData;
			}
			catch
			{
				MessageBox.Show("Unable to check for updates", "Error");
				return "";
			}
		}
	}
}
