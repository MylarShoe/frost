﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
	public partial class FrostGUI
	{
		
		private void quitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if (FileList.Items.Count == 0)
			{
				if (RenderChecker() == 1)
				{
					if (MessageBox.Show("Are you sure you want to exit?\nThis will cancel the current render", "Confirm Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No) { e.Cancel = true; return; }
					else 
					{
						try { _cmd.Kill(); }
						catch (InvalidOperationException) { }
						e.Cancel = false; 
						return; 
					}
				}
				else
				{
					Application.Exit();
				}
			}
			else
			{
				if (RenderChecker() == 1)
				{
					if (MessageBox.Show("Are you sure you want to exit?\nThis will cancel the current render", "Confirm Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
					else
					{
						try { _cmd.Kill(); }
						catch (InvalidOperationException) { }
						e.Cancel = false;
						return;
					}
				}
				else
				{
					if (MessageBox.Show("You have a file in Queue\nAre you sure you want to exit?", "Confirm Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
					else
					{
						e.Cancel = false;
						return;
					}
				}
			}
		}
	}
}
