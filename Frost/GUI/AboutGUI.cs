﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frost
{
    public partial class AboutGUI : Form
    {
        public AboutGUI()
        {
            InitializeComponent();
			Copyright.SelectAll();
			Copyright.SelectionAlignment = HorizontalAlignment.Center;
			version.Text = versionNum();
        }
		
		private void Copyright_LinkClicked(object sender, System.Windows.Forms.LinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}

		private string versionNum()
		{
			Version version = new Version(Application.ProductVersion);
			string MajMin = version.Major + "." + version.Minor;
			return "version " + MajMin;
		}
    }
}
