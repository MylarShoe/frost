﻿namespace Frost
{
	partial class GuideGUI
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GuideGUI));
			this.GuideWindow = new System.Windows.Forms.RichTextBox();
			this.Guide_Close = new System.Windows.Forms.Button();
			this.FFmpeg = new System.Windows.Forms.Button();
			this.ProRes = new System.Windows.Forms.Button();
			this.H265 = new System.Windows.Forms.Button();
			this.General = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// GuideWindow
			// 
			this.GuideWindow.BackColor = System.Drawing.SystemColors.Menu;
			this.GuideWindow.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.GuideWindow.Location = new System.Drawing.Point(12, 49);
			this.GuideWindow.Name = "GuideWindow";
			this.GuideWindow.ReadOnly = true;
			this.GuideWindow.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.GuideWindow.Size = new System.Drawing.Size(472, 295);
			this.GuideWindow.TabIndex = 0;
			this.GuideWindow.Text = "Click an Option above for More information on How To Use Frost";
			this.GuideWindow.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.GuideWindow_LinkClicked);
			// 
			// Guide_Close
			// 
			this.Guide_Close.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Guide_Close.Location = new System.Drawing.Point(379, 350);
			this.Guide_Close.Name = "Guide_Close";
			this.Guide_Close.Size = new System.Drawing.Size(105, 23);
			this.Guide_Close.TabIndex = 1;
			this.Guide_Close.Text = "Close";
			this.Guide_Close.UseVisualStyleBackColor = true;
			// 
			// FFmpeg
			// 
			this.FFmpeg.Location = new System.Drawing.Point(130, 12);
			this.FFmpeg.Name = "FFmpeg";
			this.FFmpeg.Size = new System.Drawing.Size(113, 23);
			this.FFmpeg.TabIndex = 2;
			this.FFmpeg.Text = "Installing FFmpeg";
			this.FFmpeg.UseVisualStyleBackColor = true;
			this.FFmpeg.Click += new System.EventHandler(this.FFmpeg_Click);
			// 
			// ProRes
			// 
			this.ProRes.Location = new System.Drawing.Point(251, 12);
			this.ProRes.Name = "ProRes";
			this.ProRes.Size = new System.Drawing.Size(113, 23);
			this.ProRes.TabIndex = 3;
			this.ProRes.Text = "Rendering ProRes";
			this.ProRes.UseVisualStyleBackColor = true;
			this.ProRes.Click += new System.EventHandler(this.ProRes_Click);
			// 
			// H265
			// 
			this.H265.Location = new System.Drawing.Point(371, 12);
			this.H265.Name = "H265";
			this.H265.Size = new System.Drawing.Size(113, 23);
			this.H265.TabIndex = 4;
			this.H265.Text = "Rendering H.265";
			this.H265.UseVisualStyleBackColor = true;
			this.H265.Click += new System.EventHandler(this.H265_Click);
			// 
			// General
			// 
			this.General.Location = new System.Drawing.Point(12, 12);
			this.General.Name = "General";
			this.General.Size = new System.Drawing.Size(113, 23);
			this.General.TabIndex = 5;
			this.General.Text = "General";
			this.General.UseVisualStyleBackColor = true;
			this.General.Click += new System.EventHandler(this.General_Click);
			// 
			// GuideGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(496, 383);
			this.Controls.Add(this.General);
			this.Controls.Add(this.H265);
			this.Controls.Add(this.ProRes);
			this.Controls.Add(this.FFmpeg);
			this.Controls.Add(this.Guide_Close);
			this.Controls.Add(this.GuideWindow);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GuideGUI";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Guide";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.RichTextBox GuideWindow;
		private System.Windows.Forms.Button Guide_Close;
		private System.Windows.Forms.Button FFmpeg;
		private System.Windows.Forms.Button ProRes;
		private System.Windows.Forms.Button H265;
		private System.Windows.Forms.Button General;
	}
}