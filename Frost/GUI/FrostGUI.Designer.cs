﻿namespace Frost
{
    partial class FrostGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrostGUI));
			this.MenuBar = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.batchModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.resetDefaultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearOutputWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.guidsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.changeLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.ProRes = new System.Windows.Forms.TabPage();
			this.PR_Audio_Text = new System.Windows.Forms.TextBox();
			this.PR_Audio = new System.Windows.Forms.ComboBox();
			this.PR_Resolution = new System.Windows.Forms.ComboBox();
			this.PR_Resolution_Text = new System.Windows.Forms.TextBox();
			this.PR_Quality_Text = new System.Windows.Forms.TextBox();
			this.PR_Quality = new System.Windows.Forms.ComboBox();
			this.H265 = new System.Windows.Forms.TabPage();
			this.MKV_Button = new System.Windows.Forms.RadioButton();
			this.MP4_Button = new System.Windows.Forms.RadioButton();
			this.H_Quality = new System.Windows.Forms.NumericUpDown();
			this.RenderSpeed = new System.Windows.Forms.TextBox();
			this.H_Speed = new System.Windows.Forms.ComboBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.H_Audio = new System.Windows.Forms.ComboBox();
			this.H_Quality_Text = new System.Windows.Forms.TextBox();
			this.H_Resolution_Text = new System.Windows.Forms.TextBox();
			this.H_Resolution = new System.Windows.Forms.ComboBox();
			this.StartButton = new System.Windows.Forms.Button();
			this.FileList = new System.Windows.Forms.ListBox();
			this.AddFiles = new System.Windows.Forms.Button();
			this.RemoveFiles = new System.Windows.Forms.Button();
			this.Destination_Select = new System.Windows.Forms.Button();
			this.DestinationList = new System.Windows.Forms.ListBox();
			this.DestinationLock = new System.Windows.Forms.CheckBox();
			this.DestinationClear = new System.Windows.Forms.Button();
			this.CRF_Popup = new System.Windows.Forms.ToolTip(this.components);
			this.OpenOutput = new System.Windows.Forms.Button();
			this.CommandWindow = new System.Windows.Forms.ListBox();
			this.CurrentRender = new System.Windows.Forms.TextBox();
			this.MenuBar.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.ProRes.SuspendLayout();
			this.H265.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.H_Quality)).BeginInit();
			this.SuspendLayout();
			// 
			// MenuBar
			// 
			this.MenuBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MenuBar.AutoSize = false;
			this.MenuBar.Dock = System.Windows.Forms.DockStyle.None;
			this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.MenuBar.Location = new System.Drawing.Point(0, 0);
			this.MenuBar.Name = "MenuBar";
			this.MenuBar.Size = new System.Drawing.Size(590, 24);
			this.MenuBar.TabIndex = 0;
			this.MenuBar.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.batchModeToolStripMenuItem,
            this.toolStripSeparator2,
            this.quitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// batchModeToolStripMenuItem
			// 
			this.batchModeToolStripMenuItem.Name = "batchModeToolStripMenuItem";
			this.batchModeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
			this.batchModeToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
			this.batchModeToolStripMenuItem.Text = "Batch Mode";
			this.batchModeToolStripMenuItem.Click += new System.EventHandler(this.batchModeToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(176, 6);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
			this.quitToolStripMenuItem.Text = "Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetDefaultsToolStripMenuItem,
            this.clearToolStripMenuItem,
            this.clearOutputWindowToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// resetDefaultsToolStripMenuItem
			// 
			this.resetDefaultsToolStripMenuItem.Name = "resetDefaultsToolStripMenuItem";
			this.resetDefaultsToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
			this.resetDefaultsToolStripMenuItem.Text = "Reset Defaults";
			this.resetDefaultsToolStripMenuItem.Click += new System.EventHandler(this.resetDefaultsToolStripMenuItem_Click);
			// 
			// clearToolStripMenuItem
			// 
			this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
			this.clearToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
			this.clearToolStripMenuItem.Text = "Clear File Lists";
			this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
			// 
			// clearOutputWindowToolStripMenuItem
			// 
			this.clearOutputWindowToolStripMenuItem.Name = "clearOutputWindowToolStripMenuItem";
			this.clearOutputWindowToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
			this.clearOutputWindowToolStripMenuItem.Text = "Clear Output Window";
			this.clearOutputWindowToolStripMenuItem.Click += new System.EventHandler(this.clearOutputWindowToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.guidsToolStripMenuItem,
            this.toolStripSeparator1,
            this.changeLogToolStripMenuItem,
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// guidsToolStripMenuItem
			// 
			this.guidsToolStripMenuItem.Name = "guidsToolStripMenuItem";
			this.guidsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
			this.guidsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.guidsToolStripMenuItem.Text = "Guide";
			this.guidsToolStripMenuItem.Click += new System.EventHandler(this.guidsToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(135, 6);
			// 
			// changeLogToolStripMenuItem
			// 
			this.changeLogToolStripMenuItem.Name = "changeLogToolStripMenuItem";
			this.changeLogToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.changeLogToolStripMenuItem.Text = "Change Log";
			this.changeLogToolStripMenuItem.Click += new System.EventHandler(this.changeLogToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.tabControl1.Controls.Add(this.ProRes);
			this.tabControl1.Controls.Add(this.H265);
			this.tabControl1.Location = new System.Drawing.Point(16, 164);
			this.tabControl1.MaximumSize = new System.Drawing.Size(758, 114);
			this.tabControl1.MinimumSize = new System.Drawing.Size(558, 114);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(558, 114);
			this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
			this.tabControl1.TabIndex = 1;
			// 
			// ProRes
			// 
			this.ProRes.Controls.Add(this.PR_Audio_Text);
			this.ProRes.Controls.Add(this.PR_Audio);
			this.ProRes.Controls.Add(this.PR_Resolution);
			this.ProRes.Controls.Add(this.PR_Resolution_Text);
			this.ProRes.Controls.Add(this.PR_Quality_Text);
			this.ProRes.Controls.Add(this.PR_Quality);
			this.ProRes.Location = new System.Drawing.Point(4, 22);
			this.ProRes.Name = "ProRes";
			this.ProRes.Padding = new System.Windows.Forms.Padding(3);
			this.ProRes.Size = new System.Drawing.Size(550, 88);
			this.ProRes.TabIndex = 0;
			this.ProRes.Text = "ProRes";
			this.ProRes.UseVisualStyleBackColor = true;
			// 
			// PR_Audio_Text
			// 
			this.PR_Audio_Text.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.PR_Audio_Text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.PR_Audio_Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.PR_Audio_Text.Location = new System.Drawing.Point(202, 14);
			this.PR_Audio_Text.Name = "PR_Audio_Text";
			this.PR_Audio_Text.ReadOnly = true;
			this.PR_Audio_Text.Size = new System.Drawing.Size(146, 13);
			this.PR_Audio_Text.TabIndex = 5;
			this.PR_Audio_Text.Text = "Audio";
			// 
			// PR_Audio
			// 
			this.PR_Audio.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.PR_Audio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.PR_Audio.FormattingEnabled = true;
			this.PR_Audio.Items.AddRange(new object[] {
            "Unchanged",
            "Remove"});
			this.PR_Audio.Location = new System.Drawing.Point(202, 40);
			this.PR_Audio.Name = "PR_Audio";
			this.PR_Audio.Size = new System.Drawing.Size(146, 21);
			this.PR_Audio.TabIndex = 4;
			// 
			// PR_Resolution
			// 
			this.PR_Resolution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.PR_Resolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.PR_Resolution.FormattingEnabled = true;
			this.PR_Resolution.Items.AddRange(new object[] {
            "Unscaled",
            "720 HD (1280x720)",
            "1080 HD (1920x1080)",
            "UHD (3840x2160)",
            "4K (4096x2160)"});
			this.PR_Resolution.Location = new System.Drawing.Point(376, 40);
			this.PR_Resolution.Name = "PR_Resolution";
			this.PR_Resolution.Size = new System.Drawing.Size(146, 21);
			this.PR_Resolution.TabIndex = 3;
			// 
			// PR_Resolution_Text
			// 
			this.PR_Resolution_Text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.PR_Resolution_Text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.PR_Resolution_Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.PR_Resolution_Text.Location = new System.Drawing.Point(376, 14);
			this.PR_Resolution_Text.Name = "PR_Resolution_Text";
			this.PR_Resolution_Text.ReadOnly = true;
			this.PR_Resolution_Text.Size = new System.Drawing.Size(146, 13);
			this.PR_Resolution_Text.TabIndex = 2;
			this.PR_Resolution_Text.Text = "Resolution";
			// 
			// PR_Quality_Text
			// 
			this.PR_Quality_Text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.PR_Quality_Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.PR_Quality_Text.Location = new System.Drawing.Point(28, 14);
			this.PR_Quality_Text.Name = "PR_Quality_Text";
			this.PR_Quality_Text.ReadOnly = true;
			this.PR_Quality_Text.Size = new System.Drawing.Size(146, 13);
			this.PR_Quality_Text.TabIndex = 1;
			this.PR_Quality_Text.Text = "Quality";
			// 
			// PR_Quality
			// 
			this.PR_Quality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.PR_Quality.Items.AddRange(new object[] {
            "422 Proxy",
            "422 LT",
            "422",
            "422 HQ",
            "4444 (No Alpha)",
            "4444 (8-bit Alpha)",
            "4444 (16-bit Alpha)"});
			this.PR_Quality.Location = new System.Drawing.Point(28, 40);
			this.PR_Quality.Name = "PR_Quality";
			this.PR_Quality.Size = new System.Drawing.Size(146, 21);
			this.PR_Quality.TabIndex = 0;
			// 
			// H265
			// 
			this.H265.Controls.Add(this.MKV_Button);
			this.H265.Controls.Add(this.MP4_Button);
			this.H265.Controls.Add(this.H_Quality);
			this.H265.Controls.Add(this.RenderSpeed);
			this.H265.Controls.Add(this.H_Speed);
			this.H265.Controls.Add(this.textBox1);
			this.H265.Controls.Add(this.H_Audio);
			this.H265.Controls.Add(this.H_Quality_Text);
			this.H265.Controls.Add(this.H_Resolution_Text);
			this.H265.Controls.Add(this.H_Resolution);
			this.H265.Location = new System.Drawing.Point(4, 22);
			this.H265.Name = "H265";
			this.H265.Padding = new System.Windows.Forms.Padding(3);
			this.H265.Size = new System.Drawing.Size(550, 88);
			this.H265.TabIndex = 1;
			this.H265.Text = "H.265";
			this.H265.UseVisualStyleBackColor = true;
			// 
			// MKV_Button
			// 
			this.MKV_Button.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.MKV_Button.AutoSize = true;
			this.MKV_Button.Location = new System.Drawing.Point(265, 61);
			this.MKV_Button.Name = "MKV_Button";
			this.MKV_Button.Size = new System.Drawing.Size(67, 17);
			this.MKV_Button.TabIndex = 11;
			this.MKV_Button.TabStop = true;
			this.MKV_Button.Text = "MKV File";
			this.MKV_Button.UseVisualStyleBackColor = true;
			// 
			// MP4_Button
			// 
			this.MP4_Button.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.MP4_Button.AutoSize = true;
			this.MP4_Button.Location = new System.Drawing.Point(190, 61);
			this.MP4_Button.Name = "MP4_Button";
			this.MP4_Button.Size = new System.Drawing.Size(66, 17);
			this.MP4_Button.TabIndex = 10;
			this.MP4_Button.TabStop = true;
			this.MP4_Button.Text = "MP4 File";
			this.MP4_Button.UseVisualStyleBackColor = true;
			// 
			// H_Quality
			// 
			this.H_Quality.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.H_Quality.Location = new System.Drawing.Point(28, 31);
			this.H_Quality.Maximum = new decimal(new int[] {
            51,
            0,
            0,
            0});
			this.H_Quality.Name = "H_Quality";
			this.H_Quality.Size = new System.Drawing.Size(65, 20);
			this.H_Quality.TabIndex = 9;
			this.H_Quality.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// RenderSpeed
			// 
			this.RenderSpeed.AutoCompleteCustomSource.AddRange(new string[] {
            "Render Speed"});
			this.RenderSpeed.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.RenderSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.RenderSpeed.CausesValidation = false;
			this.RenderSpeed.Location = new System.Drawing.Point(114, 11);
			this.RenderSpeed.Name = "RenderSpeed";
			this.RenderSpeed.ReadOnly = true;
			this.RenderSpeed.Size = new System.Drawing.Size(100, 13);
			this.RenderSpeed.TabIndex = 8;
			this.RenderSpeed.Text = "Render Speed";
			// 
			// H_Speed
			// 
			this.H_Speed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.H_Speed.FormattingEnabled = true;
			this.H_Speed.Items.AddRange(new object[] {
            "ultrafast",
            "superfast",
            "veryfast",
            "faster",
            "fast",
            "medium (default)",
            "slow",
            "slower",
            "veryslow",
            "placebo"});
			this.H_Speed.Location = new System.Drawing.Point(114, 31);
			this.H_Speed.Name = "H_Speed";
			this.H_Speed.Size = new System.Drawing.Size(117, 21);
			this.H_Speed.TabIndex = 7;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.BackColor = System.Drawing.SystemColors.HighlightText;
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Location = new System.Drawing.Point(254, 11);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(87, 13);
			this.textBox1.TabIndex = 6;
			this.textBox1.Text = "Audio";
			// 
			// H_Audio
			// 
			this.H_Audio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.H_Audio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.H_Audio.FormattingEnabled = true;
			this.H_Audio.Items.AddRange(new object[] {
            "Unchanged",
            "Remove"});
			this.H_Audio.Location = new System.Drawing.Point(254, 31);
			this.H_Audio.Name = "H_Audio";
			this.H_Audio.Size = new System.Drawing.Size(117, 21);
			this.H_Audio.TabIndex = 5;
			// 
			// H_Quality_Text
			// 
			this.H_Quality_Text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.H_Quality_Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.H_Quality_Text.Location = new System.Drawing.Point(28, 11);
			this.H_Quality_Text.Name = "H_Quality_Text";
			this.H_Quality_Text.ReadOnly = true;
			this.H_Quality_Text.Size = new System.Drawing.Size(68, 13);
			this.H_Quality_Text.TabIndex = 3;
			this.H_Quality_Text.Text = "CRF Value";
			// 
			// H_Resolution_Text
			// 
			this.H_Resolution_Text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.H_Resolution_Text.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.H_Resolution_Text.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.H_Resolution_Text.Location = new System.Drawing.Point(394, 11);
			this.H_Resolution_Text.Name = "H_Resolution_Text";
			this.H_Resolution_Text.ReadOnly = true;
			this.H_Resolution_Text.Size = new System.Drawing.Size(128, 13);
			this.H_Resolution_Text.TabIndex = 1;
			this.H_Resolution_Text.Text = "Resolution";
			// 
			// H_Resolution
			// 
			this.H_Resolution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.H_Resolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.H_Resolution.FormattingEnabled = true;
			this.H_Resolution.Items.AddRange(new object[] {
            "Unscaled",
            "720 HD (1280x720)",
            "1080 HD (1920x1080)",
            "UHD (3840x2160)",
            "4K (4096x2160)"});
			this.H_Resolution.Location = new System.Drawing.Point(394, 32);
			this.H_Resolution.Name = "H_Resolution";
			this.H_Resolution.Size = new System.Drawing.Size(128, 21);
			this.H_Resolution.TabIndex = 0;
			// 
			// StartButton
			// 
			this.StartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.StartButton.Location = new System.Drawing.Point(448, 286);
			this.StartButton.Name = "StartButton";
			this.StartButton.Size = new System.Drawing.Size(130, 23);
			this.StartButton.TabIndex = 2;
			this.StartButton.Text = "Start";
			this.StartButton.UseVisualStyleBackColor = true;
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// FileList
			// 
			this.FileList.AllowDrop = true;
			this.FileList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FileList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FileList.FormattingEnabled = true;
			this.FileList.HorizontalScrollbar = true;
			this.FileList.ItemHeight = 15;
			this.FileList.Location = new System.Drawing.Point(16, 37);
			this.FileList.Name = "FileList";
			this.FileList.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.FileList.Size = new System.Drawing.Size(558, 19);
			this.FileList.TabIndex = 3;
			// 
			// AddFiles
			// 
			this.AddFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.AddFiles.Location = new System.Drawing.Point(414, 62);
			this.AddFiles.Name = "AddFiles";
			this.AddFiles.Size = new System.Drawing.Size(88, 23);
			this.AddFiles.TabIndex = 5;
			this.AddFiles.Text = "Add File";
			this.AddFiles.UseVisualStyleBackColor = true;
			this.AddFiles.Click += new System.EventHandler(this.AddFiles_Click);
			// 
			// RemoveFiles
			// 
			this.RemoveFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.RemoveFiles.Location = new System.Drawing.Point(508, 62);
			this.RemoveFiles.Name = "RemoveFiles";
			this.RemoveFiles.Size = new System.Drawing.Size(66, 23);
			this.RemoveFiles.TabIndex = 6;
			this.RemoveFiles.Text = "Clear";
			this.RemoveFiles.UseVisualStyleBackColor = true;
			this.RemoveFiles.Click += new System.EventHandler(this.RemoveFiles_Click);
			// 
			// Destination_Select
			// 
			this.Destination_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.Destination_Select.Location = new System.Drawing.Point(414, 123);
			this.Destination_Select.Name = "Destination_Select";
			this.Destination_Select.Size = new System.Drawing.Size(88, 23);
			this.Destination_Select.TabIndex = 8;
			this.Destination_Select.Text = "Set Destination";
			this.Destination_Select.UseVisualStyleBackColor = true;
			this.Destination_Select.Click += new System.EventHandler(this.Destination_Select_Click);
			// 
			// DestinationList
			// 
			this.DestinationList.AllowDrop = true;
			this.DestinationList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DestinationList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DestinationList.FormattingEnabled = true;
			this.DestinationList.ItemHeight = 15;
			this.DestinationList.Location = new System.Drawing.Point(16, 98);
			this.DestinationList.Name = "DestinationList";
			this.DestinationList.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.DestinationList.Size = new System.Drawing.Size(558, 19);
			this.DestinationList.TabIndex = 9;
			// 
			// DestinationLock
			// 
			this.DestinationLock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.DestinationLock.AutoSize = true;
			this.DestinationLock.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.DestinationLock.Location = new System.Drawing.Point(291, 127);
			this.DestinationLock.Name = "DestinationLock";
			this.DestinationLock.Size = new System.Drawing.Size(117, 17);
			this.DestinationLock.TabIndex = 10;
			this.DestinationLock.Text = "Lock Output Folder";
			this.DestinationLock.UseVisualStyleBackColor = true;
			// 
			// DestinationClear
			// 
			this.DestinationClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.DestinationClear.Location = new System.Drawing.Point(508, 123);
			this.DestinationClear.Name = "DestinationClear";
			this.DestinationClear.Size = new System.Drawing.Size(66, 23);
			this.DestinationClear.TabIndex = 11;
			this.DestinationClear.Text = "Clear";
			this.DestinationClear.UseVisualStyleBackColor = true;
			this.DestinationClear.Click += new System.EventHandler(this.DestinationClear_Click);
			// 
			// OpenOutput
			// 
			this.OpenOutput.Location = new System.Drawing.Point(16, 121);
			this.OpenOutput.Name = "OpenOutput";
			this.OpenOutput.Size = new System.Drawing.Size(99, 23);
			this.OpenOutput.TabIndex = 14;
			this.OpenOutput.Text = "Open Destination";
			this.OpenOutput.UseVisualStyleBackColor = true;
			this.OpenOutput.Click += new System.EventHandler(this.OpenOutput_Click);
			// 
			// CommandWindow
			// 
			this.CommandWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CommandWindow.BackColor = System.Drawing.SystemColors.WindowText;
			this.CommandWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CommandWindow.ForeColor = System.Drawing.Color.Lime;
			this.CommandWindow.FormattingEnabled = true;
			this.CommandWindow.ItemHeight = 15;
			this.CommandWindow.Location = new System.Drawing.Point(0, 338);
			this.CommandWindow.Name = "CommandWindow";
			this.CommandWindow.Size = new System.Drawing.Size(590, 109);
			this.CommandWindow.TabIndex = 12;
			// 
			// CurrentRender
			// 
			this.CurrentRender.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CurrentRender.BackColor = System.Drawing.SystemColors.HighlightText;
			this.CurrentRender.Location = new System.Drawing.Point(0, 315);
			this.CurrentRender.Name = "CurrentRender";
			this.CurrentRender.ReadOnly = true;
			this.CurrentRender.ShortcutsEnabled = false;
			this.CurrentRender.Size = new System.Drawing.Size(590, 20);
			this.CurrentRender.TabIndex = 13;
			this.CurrentRender.WordWrap = false;
			// 
			// FrostGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(590, 447);
			this.Controls.Add(this.CurrentRender);
			this.Controls.Add(this.OpenOutput);
			this.Controls.Add(this.DestinationClear);
			this.Controls.Add(this.CommandWindow);
			this.Controls.Add(this.DestinationLock);
			this.Controls.Add(this.DestinationList);
			this.Controls.Add(this.Destination_Select);
			this.Controls.Add(this.RemoveFiles);
			this.Controls.Add(this.AddFiles);
			this.Controls.Add(this.FileList);
			this.Controls.Add(this.StartButton);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.MenuBar);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.MenuBar;
			this.MinimumSize = new System.Drawing.Size(606, 486);
			this.Name = "FrostGUI";
			this.Text = "Frost";
			this.Load += new System.EventHandler(this.FrostGUI_Load);
			this.MenuBar.ResumeLayout(false);
			this.MenuBar.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.ProRes.ResumeLayout(false);
			this.ProRes.PerformLayout();
			this.H265.ResumeLayout(false);
			this.H265.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.H_Quality)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuBar;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guidsToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage ProRes;
        private System.Windows.Forms.TabPage H265;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.ComboBox PR_Quality;
        private System.Windows.Forms.TextBox PR_Resolution_Text;
        private System.Windows.Forms.TextBox PR_Quality_Text;
        private System.Windows.Forms.ComboBox PR_Resolution;
		private System.Windows.Forms.ListBox FileList;
        private System.Windows.Forms.Button AddFiles;
        private System.Windows.Forms.Button RemoveFiles;
        private System.Windows.Forms.TextBox H_Resolution_Text;
		private System.Windows.Forms.ComboBox H_Resolution;
        private System.Windows.Forms.TextBox H_Quality_Text;
        private System.Windows.Forms.Button Destination_Select;
        private System.Windows.Forms.ToolStripMenuItem batchModeToolStripMenuItem;
        private System.Windows.Forms.ListBox DestinationList;
        private System.Windows.Forms.CheckBox DestinationLock;
		private System.Windows.Forms.Button DestinationClear;
        private System.Windows.Forms.TextBox PR_Audio_Text;
        private System.Windows.Forms.ComboBox PR_Audio;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox H_Audio;
		private System.Windows.Forms.ToolTip CRF_Popup;
		private System.Windows.Forms.ComboBox H_Speed;
		private System.Windows.Forms.TextBox RenderSpeed;
		private System.Windows.Forms.NumericUpDown H_Quality;
		private System.Windows.Forms.RadioButton MKV_Button;
		private System.Windows.Forms.RadioButton MP4_Button;
		private System.Windows.Forms.Button OpenOutput;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem changeLogToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem resetDefaultsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearOutputWindowToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ListBox CommandWindow;
		private System.Windows.Forms.TextBox CurrentRender;
    }
}

