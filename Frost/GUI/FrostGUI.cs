﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Frost
{
    public partial class FrostGUI : Form
    {
        public FrostGUI()
        {
            InitializeComponent();
			this.FileList.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileList_DragDrop);
			this.FileList.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileList_DragEnter);
			this.DestinationList.DragDrop += new System.Windows.Forms.DragEventHandler(this.DestinationList_DragDrop);
			this.DestinationList.DragEnter += new System.Windows.Forms.DragEventHandler(this.DestinationList_DragEnter);
        }

        private void FrostGUI_Load(object sender, EventArgs e)
        {
			if (!File.Exists("ffmpeg.exe"))
			{
				MessageBox.Show("FFmpeg binary not found. Check the Guide for\ninstructions on how to install it.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			SetDefaults();

			//Version version = new Version(Application.ProductVersion);
			//MessageBox.Show(version.ToString());

			//Updater.CheckUpdate();
        }

        private void AddFiles_Click(object sender, EventArgs e)
        {
            OpenFileDialog FileSelectDialog = new OpenFileDialog();

            FileSelectDialog.Filter = "Video Files(*.MOV;*.AVI;*.MP4;*.MXF)|*.MOV;*.AVI;*.MP4;*.MXF|All files (*.*)|*.*";
            FileSelectDialog.CheckFileExists = true;
            FileSelectDialog.Multiselect = false;

            if (FileSelectDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in FileSelectDialog.FileNames)
                {
                    string FilePath = Path.GetDirectoryName(file);
                    if (FileList.Items.Contains(file) == true)
                    {
                        MessageBox.Show("File is already in Queue", "Error");
                        continue;
                    }
                    else
                    {
                        if (FileList.Items.Count > 0)
                        {
                            FileList.Items.Clear();
                            FileList.Items.Add(file);
                        }
                        else 
                        { 
                            FileList.Items.Add(file); 
                        }
                        if (DestinationLock.Checked == false)
                        {
                            DestinationList.Items.Clear();
                            DestinationList.Items.Add(FilePath);
                        }
                    }
                }
            }
        }

        private void RemoveFiles_Click(object sender, EventArgs e)
        {
			if (FileList.Items.Count > 0)
			{
				FileList.Items.Clear();
			}
			else
			{
				MessageBox.Show("No File In Queue", "Error");
			}
        }

        private void Destination_Select_Click(object sender, EventArgs e)
        {
            if (DestinationLock.Checked == true)
            {
                MessageBox.Show("Output Folder is Locked", "Error");
            }
            else
            {
                FolderBrowserDialog destFolderSelectDialog = new FolderBrowserDialog();
                destFolderSelectDialog.ShowDialog();
                string selected = destFolderSelectDialog.SelectedPath;

                DestinationList.Items.Clear();
                DestinationList.Items.Add(selected);
            }
        }

        private void DestinationClear_Click(object sender, EventArgs e)
        {
            if (DestinationLock.Checked == true)
            {
                MessageBox.Show("Output Folder is Locked", "Error");
            }
            else
            {
                DestinationList.Items.Clear();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutGUI about = new AboutGUI();
            DialogResult dr = new DialogResult();
            dr = about.ShowDialog();
            if (dr == DialogResult.OK)
            {
                return;
            }
        }

		private void guidsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			GuideGUI guide = new GuideGUI();
			DialogResult dr = new DialogResult();
			dr = guide.ShowDialog();
			if (dr == DialogResult.OK)
			{
				return;
			}
		}

        private void batchModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
			MessageBox.Show("Under Development", "");
        }

		private void OpenOutput_Click(object sender, EventArgs e)
		{
			if (DestinationList.Items.Count == 0) { }
			else { String path = (string)DestinationList.Items[0]; Process.Start("explorer.exe", @path); }
		}

		private void changeLogToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ChangeLogGUI log = new ChangeLogGUI();
			DialogResult dr = new DialogResult();
			dr = log.ShowDialog();
			if (dr == DialogResult.OK)
			{
				return;
			}
		}

		private void resetDefaultsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SetDefaults();
		}

		private void clearToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FileList.Items.Clear();
			DestinationList.Items.Clear();
		}

		private void clearOutputWindowToolStripMenuItem_Click(object sender, EventArgs e)
		{
			CommandWindow.Items.Clear();
		}
    }
}