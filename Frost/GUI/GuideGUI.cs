﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Frost
{
	public partial class GuideGUI : Form
	{
		public GuideGUI()
		{
			InitializeComponent();
		}

		private void FFmpeg_Click(object sender, EventArgs e)
		{
			GuideWindow.Text = "Installing FFmpeg\n\nFFmpeg is not included with the executable of Frost. There are a couple of reasons for that, but needless to say it does not come with the executable. I tried to set it up as simply as possible for you. All you need to do is download the FFmpeg.exe file, and place that file in the same folder as Frost.exe. From there everything will work just fine. While you can get a copy of FFmpeg.exe from where ever you like, but all the testing I did was using the Zeranoe build. Which you can download from here:\n\nhttp://ffmpeg.zeranoe.com/builds/\n\nDownload either the 32-bit or 64-bit Static build. If you aren’t sure which one you need just download the 32-bit version. You will also need something that can extract a 7-zip file. If you don’t have one\n\nhttp://www.7-zip.org/\n\nOnce you extract the file you will find ffmpeg.exe in the bin folder. Copy the file, and place it in the same folder as where ever you put the Frost.exe. Open Frost and it should find FFmpeg and be ready to go. Enjoy!";
		}
		private void GuideWindow_LinkClicked(object sender, System.Windows.Forms.LinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}

		private void General_Click(object sender, EventArgs e)
		{
			GuideWindow.Text = "General Guidelines\n\nFrost is the simplest way to render ProRes 422 files and H.265 files on Windows. It has been designed to give you a lot of control without having to know all the command line flags to render using FFmpeg.\n\nTo get started add a file to the source file window. You can do this by clicking the Add File button, navigating to the desired file, and clicking open. When you do this it will automatically populate the destination folder with the folder the source file originated from. You can change this by clicking on Set Destination. If you wish that the destination folder does not change when selecting another file, you can check the Lock Output Folder.\n\nOnce all the settings are as you desire click Start. The output from the render will be displayed in the box at the bottom of the screen.";
		}

		private void ProRes_Click(object sender, EventArgs e)
		{
			GuideWindow.Text = "ProRes Settings\n\nCurrently Frost only supports .mov files for conversion to ProRes\n\nFrost is only capable of rendering files into ProRes 422 formats only. In the Quality dropdown you will be able to select which version of 422 you would like. This includes Proxy, LT, Standard 422, and HQ.\n\nThe Audio Dropdown allows you to leave the audio as is from the source file and copy it, or completely remove it leaving the video with no audio track.\n\nYou also the option to change the resolution of the output file. Right now, only standard resolutions are supported, but expect user entered resolutions to come in the near future.";
		}

		private void H265_Click(object sender, EventArgs e)
		{
			GuideWindow.Text = "H.265 (HEVC) Settings\n\nCurrently Frost only supports .mp4 files for conversion to H.265\n\nH.265 (HEVC) is the up and coming format for online file streaming/delivery. Because it is so new, Frost has some limitations in the ways it can render into H.265. The biggest limitation is that in order for the render to complete successfully the input file must be an .mp4 file.\n**Coming Soon** Frost tries to work around this by rendering any file not in an .mp4 file first into an .mp4 file, and then rendering that intermediate file into an H.265 (HEVC) file.\n\nH.265 includes a different set of render parameters. The first being CRF which is the Constant Rate Factor. It is equivocal to a variable bit rate that keeps a constant quality while ensuring the file size is balanced. The values accepted range from 0 to 51. The lower the number the higher the quality, or more lossless meaning larger file sizes. The higher the number the lower the quality, and thus the smaller the file size. The default is 28, but the best results I have seen are between 15-22.\n\nRender Speed is a variable used to determine how much time is spent on predicting the compression ratio. Slower will spend more time, and can give a higher quality, while fast will render faster, it will result in a lower quality. Results may vary, so medium (default) should be your starting place. You can adjust from there.\n\nThe Audio Dropdown allows you to leave the audio as is from the source file and copy it, or completely remove it leaving the video with no audio track.\n\nYou also the option to change the resolution of the output file. Right now, only standard resolutions are supported, but expect user entered resolutions to come in the near future.\n\nH.265 files can be encapsulated inside of either .mp4 files or .mkv files. MKV files will play by default in VLC player. The MP4 will be encoded correctly, but it will not play in VLC. It will play inside of Media Player Classic.";

		}
	}
}
