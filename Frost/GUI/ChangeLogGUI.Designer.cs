﻿namespace Frost
{
	partial class ChangeLogGUI
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeLogGUI));
			this.ChangeClose = new System.Windows.Forms.Button();
			this.ChangeLogText = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// ChangeClose
			// 
			this.ChangeClose.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ChangeClose.Location = new System.Drawing.Point(304, 245);
			this.ChangeClose.Name = "ChangeClose";
			this.ChangeClose.Size = new System.Drawing.Size(75, 23);
			this.ChangeClose.TabIndex = 0;
			this.ChangeClose.Text = "Close";
			this.ChangeClose.UseVisualStyleBackColor = true;
			// 
			// ChangeLogText
			// 
			this.ChangeLogText.BackColor = System.Drawing.SystemColors.Menu;
			this.ChangeLogText.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ChangeLogText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ChangeLogText.Location = new System.Drawing.Point(12, 12);
			this.ChangeLogText.Name = "ChangeLogText";
			this.ChangeLogText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.ChangeLogText.Size = new System.Drawing.Size(367, 227);
			this.ChangeLogText.TabIndex = 1;
			this.ChangeLogText.Text = resources.GetString("ChangeLogText.Text");
			// 
			// ChangeLogGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(391, 280);
			this.Controls.Add(this.ChangeLogText);
			this.Controls.Add(this.ChangeClose);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ChangeLogGUI";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Change Log";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button ChangeClose;
		private System.Windows.Forms.RichTextBox ChangeLogText;
	}
}