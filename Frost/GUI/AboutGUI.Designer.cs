﻿namespace Frost
{
    partial class AboutGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutGUI));
			this.Title = new System.Windows.Forms.TextBox();
			this.AboutClose = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.Copyright = new System.Windows.Forms.RichTextBox();
			this.version = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// Title
			// 
			this.Title.BackColor = System.Drawing.SystemColors.Control;
			this.Title.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Title.CausesValidation = false;
			this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title.HideSelection = false;
			this.Title.Location = new System.Drawing.Point(12, 6);
			this.Title.Name = "Title";
			this.Title.ReadOnly = true;
			this.Title.ShortcutsEnabled = false;
			this.Title.Size = new System.Drawing.Size(425, 19);
			this.Title.TabIndex = 0;
			this.Title.TabStop = false;
			this.Title.Text = "Frost";
			this.Title.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// AboutClose
			// 
			this.AboutClose.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.AboutClose.Location = new System.Drawing.Point(345, 101);
			this.AboutClose.Name = "AboutClose";
			this.AboutClose.Size = new System.Drawing.Size(92, 23);
			this.AboutClose.TabIndex = 2;
			this.AboutClose.Text = "Close";
			this.AboutClose.UseVisualStyleBackColor = true;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(84, 8);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(70, 50);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// Copyright
			// 
			this.Copyright.BackColor = System.Drawing.SystemColors.Menu;
			this.Copyright.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Copyright.Location = new System.Drawing.Point(12, 48);
			this.Copyright.Name = "Copyright";
			this.Copyright.ReadOnly = true;
			this.Copyright.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.Copyright.ShortcutsEnabled = false;
			this.Copyright.Size = new System.Drawing.Size(425, 73);
			this.Copyright.TabIndex = 4;
			this.Copyright.Text = "© 2014 Derek Reinhardt\n\nFor Comments, Bugs, Other Problems, and/or Requests\nhttp:" +
    "//derekreinhardt.com/frost_support";
			this.Copyright.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.Copyright_LinkClicked);
			// 
			// version
			// 
			this.version.BackColor = System.Drawing.SystemColors.Menu;
			this.version.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.version.Location = new System.Drawing.Point(175, 27);
			this.version.Name = "version";
			this.version.Size = new System.Drawing.Size(100, 13);
			this.version.TabIndex = 5;
			this.version.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// AboutGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(449, 136);
			this.Controls.Add(this.version);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.AboutClose);
			this.Controls.Add(this.Copyright);
			this.Controls.Add(this.Title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutGUI";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "About";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.TextBox Title;
        private System.Windows.Forms.Button AboutClose;
        private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.RichTextBox Copyright;
		private System.Windows.Forms.TextBox version;
    }
}

